package Loader;

import JobSearch.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class JobLoader {
    private JobSearch jobSearch;

    public JobLoader(){
        this.jobSearch = new JobSearch();

    }

    public JobSearch loadJob() throws IOException {
        //get the txt file
        URL path = JobLoader.class.getResource("jobinput.txt");
        File file = new File( path.getFile()); 
        BufferedReader br = new BufferedReader(new FileReader(file));
        
        //this is the number of jobs that will be added
        int n = Integer.parseInt(br.readLine());

        //the code below will read the txt file per line and create new objects
        //based on the txxt file
        for (int i = 1; i < n+1; i++ ) {
            //companyname,position,salary
            String company = br.readLine();
            String position = br.readLine();
            Double salary = Double.parseDouble(br.readLine());

            //location
            String[] locationArr = br.readLine().split(",");
            String city = locationArr[0];
            String province = locationArr[1];
            String address = locationArr[2];
            String postCode = locationArr[3];
            Location location = new Location(city, province, address, postCode);

            //requirement
            String[] requirementArr = br.readLine().split(",");
            String minWork = requirementArr[0];
            String minAge = requirementArr[1];
            String maxAge = requirementArr[2];
            String minEducation = requirementArr[3];
            String extra = requirementArr[4];
            Requirement requirements = new Requirement(minWork, minAge, maxAge, minEducation, extra);

            //contact
            String[] contactArr = br.readLine().split(",");
            String phoneNumber = contactArr[0];
            String email = contactArr[1];
            String website = contactArr[2];
            Contact contact = new Contact(phoneNumber, email, website);

            //job
            Job job = new Job(Integer.toString(i), company, salary, position, location, requirements, contact);
            this.jobSearch.addJob(job);
        }
        br.close();
        return this.jobSearch;
    }
    
}