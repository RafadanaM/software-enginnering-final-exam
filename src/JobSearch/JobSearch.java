package JobSearch;
import java.util.HashMap;
import java.util.Map;

public class JobSearch {
    
    private Map<String,Job> jobList;
    private Map<String,Job> favJobList;

    public JobSearch() {
        this.jobList = new HashMap<String,Job>();
        this.favJobList = new HashMap<String,Job>();
    }

    //this method will add the job to favourite job list
     //this method receives string that is the id of the job and return void
    public void addFavJob(String id) {
        //check if the job exist in jobList 
        if (this.jobList.containsKey(id)) {
            //if it exist then check if the job already exists in favJobList
            if (!this.favJobList.containsKey(id)) {
                this.favJobList.put(id, this.jobList.get(id));
                System.out.println("Job added to Favourite List!");
                
            } else {
                System.out.println("Job already exists in Favourite List!");
            }
        }
        //if it doesn't then it will print that the job does not exist 
        else {
            System.out.println("Job does not exist");
        } 

    }

    //this method will remove job from the favJobList
    //this method receives string that is the id of the job and return void
    public void removeFavJob(String id) {
        if (this.favJobList.remove(id) != null) {
            System.out.println("Job Removed");
            
        } else {
            System.out.println("Job ID does not exist");
        }
        
        
    }

    //this method will add job to jobList
    //this method receives string that is the id of the job and return void
    public void addJob(Job job) {
        //check if the job exists in jobList
        if (!this.jobList.containsKey(job.getId())) {
            this.jobList.put(job.getId(), job);
            System.out.println("Job added!");
        } else {
            System.out.println("Job not added!");
        }
    }
    //this method will search for job that matches to what the user input
    //this method receives an array of strings and returns a hashmap result
    public Map<String,Job> searchJob(String[] userinput) {
        Map<String,Job> result = new HashMap<String,Job>();
        boolean positionExist = true;
        boolean minSalaryExist = true;
        boolean cityExist = true;

        String position = userinput[0];
        String salary = userinput[1];
        String city = userinput[2];

        //the code below will check if the string in the elements is an empty string(meaning the user skip(press enter))
        //if it is an empty string, it will change the ...Exist variable to false
        if (position.equals("")) {
            positionExist = false;
        }

        if (salary.equals("")) {
            minSalaryExist = false;
            salary = "0";
        }

        if (city.equals("")) {
            cityExist = false;
        }
        Double minSalary = Double.parseDouble(salary);

        //iterate the jobList
        for (Job job : this.jobList.values()) {
            boolean tmp = true;
            
            if (tmp && positionExist) {
                tmp = job.getPosition().equalsIgnoreCase(position);
            }

            if (tmp && minSalaryExist) {
                tmp = job.getSalary() >= minSalary;
            }

            if (tmp && cityExist) {
                tmp = job.getLocation().getCity().equalsIgnoreCase(city);
            }

            if (tmp) {
                result.put(job.getId(), job);
            }
        }
        return result;
    }


    //return jobList
    public Map<String,Job> getJobList() {
        return this.jobList;
    }
    //return favJobList
    public Map<String,Job> getFavJobList() {
        return this.favJobList;
    }




    
}