package JobSearch;
public class Requirement {

    private String minWork;
    private String minAge;
    private String maxAge;
    private String minEducation;
    private String extra;

    public Requirement(String minWork, String minAge, String maxAge, String minEducation, String extra) {
        this.minWork = minWork;
        this.minAge = minAge;
        this.maxAge = maxAge;
        this.minEducation = minEducation;
        this.extra = extra;
    }
    
    public String toString() {
        return String.format("  Minimum Work Experience: "+this.getMinWork() + " | " + "Minimum Age: "+ this.getMinAge() + " | " + "Maximum Age: " + this.getMaxAge() + 
        " | " + "Minimum Education: " + this.getMinEducation() + " | " + "Extra Requirements: " + this.getExtra());
    }

    /**
     * @return the minWork
     */
    public String getMinWork() {
        return minWork;
    }

    /**
     * @return the minAge
     */
    public String getMinAge() {
        return minAge;
    }

    /**
     * @return the maxAge
     */
    public String getMaxAge() {
        return maxAge;
    }

    /**
     * @return the minEducation
     */
    public String getMinEducation() {
        return minEducation;
    }

    /**
     * @return the extra
     */
    public String getExtra() {
        return extra;
    }
}