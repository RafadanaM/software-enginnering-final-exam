package JobSearch;
public class Contact {

    private String phoneNumber;
    private String email;
    private String website;

    public Contact(String phoneNumber, String email, String website) {
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.website = website;
    }

    public String toString() {
        return String.format("  Phone Number: "+this.getPhoneNumber() + " | " + "Email: "+ this.getEmail() + " | " + "Website: " + this.getWebsite());

    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return website;
    }
    
}