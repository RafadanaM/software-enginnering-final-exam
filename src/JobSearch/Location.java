package JobSearch;
public class Location {
    
    private String city;
    private String province;
    private String address;
    private String postCode;

    public Location(String city, String province, String address, String postCode) {
        this.city = city;
        this.province = province;
        this.address = address;
        this.postCode = postCode;
    }

    public String toString() {
        return String.format("  City: "+this.getCity() + " | " + "Province: "+ this.getProvince() + " | " + "Address: " + this.getAddress() + " | " + "Post Code: " + this.getPostCode());
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

}