package JobSearch;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Job {
    
    private String id;
    private String company;
    private Double salary;
    private String position;
    private Location location;
    private Requirement requirements;
    private Contact contact;

    public Job(String id, String company, Double salary,String position, Location location, Requirement requirements, Contact contact) {
        this.id = id;
        this.company = company;
        this.position = position;
        this.location = location;
        this.salary = salary;
        this.requirements = requirements;
        this.contact = contact;
    }


	public String toString() {
        return String.format("ID : "+ this.getId() + "%n" +"Job Description: " + "%n" +"Company Name: "+ this.getCompany() + "%n" + "Position: "+ this.getPosition() + "%n" + "Salary: " + this.doubleToCurrency()+ 
        "%n" + "Location: " + "%n" + this.getLocation()  + "%n" + "Requirements: " + "%n" + this.getRequirements() + "%n" + "Contact: " + "%n"+ this.getContact() + "%n");
    }


    //GETTER & SETTER

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @return the contact
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @return the requirements
     */
    public Requirement getRequirements() {
        return requirements;
    }

    /**
     * @return the salary
     */
    public Double getSalary() {
        return salary;
    }

    // source : https://arbysan.wordpress.com/2014/03/02/menggunakan-format-currency-dan-format-rupiah-pada-bahasa-java/
    public String doubleToCurrency() {
        DecimalFormat indonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols rupiah = new DecimalFormatSymbols();
        rupiah.setCurrencySymbol("Rp. ");
        rupiah.setMonetaryDecimalSeparator(',');
        rupiah.setGroupingSeparator('.');
        indonesia.setDecimalFormatSymbols(rupiah);

        return indonesia.format(salary);
        
    }

    

    


    


}