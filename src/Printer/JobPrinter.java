package Printer;

import java.util.Map;

import JobSearch.Job;

public class JobPrinter {
    
    public JobPrinter() {}

    public void printJob( Map<String,Job> jobList) {
        if (jobList.size() > 0) {
            for (String key: jobList.keySet()) {
                System.out.println(jobList.get(key));
            }
        } else {
            System.out.println("List is empty");
        }
        
    }
    
}