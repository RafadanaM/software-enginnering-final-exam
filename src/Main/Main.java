package Main;
import JobSearch.*;
import Loader.JobLoader;
import Printer.JobPrinter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util. *;
public class Main {
    private static InputReader in;
    private static JobPrinter jobPrinter;
    private static JobLoader jobLoader;

    public static void main(String[] args) throws IOException {
        in = new InputReader(System.in);
        jobPrinter = new JobPrinter();
        jobLoader = new JobLoader();
        readInput();
    }

    private static void readInput() throws IOException {
        
        JobSearch jobSearch = jobLoader.loadJob();
        
        //for second iteration    
        boolean running = true;
        while (running) {
            System.out.println("COMMANDS : View Jobs = VJ, Search Jobs = SJ, Add Favourite Jobs = AFJ, Delete Favourite Job = DFJ, View Favourite Jobs = VFJ, Quit = quit" );
            System.out.print("What do you want to do?: ");
            String input = in.nextLine().toLowerCase();

            try {

                switch(input) {

                    case "vj":

                        jobPrinter.printJob(jobSearch.getJobList());
    
                        break;
                    
                    case "sj":
                        //ask for input from the user
                        System.out.println("Press Enter to Skip");
                        System.out.print("Position: ");
                        String position = in.nextLine();
                        System.out.println("Press Enter to Skip");
                        // check if the salary entered is number or empty string
                        System.out.print("Mininum Salary: ");
                        String salary = in.nextLine();
                        while(!isNumberOrEmptyString(salary)) {
                            System.out.println("Salary must be either empty or a number");
                            System.out.print("Mininum Salary: ");
                            salary = in.nextLine();
                            }
                        
                        System.out.println("Press Enter to Skip");
                        System.out.print("City: ");
                        String city = in.nextLine();

                        String[] search = {position,salary,city};
                        jobPrinter.printJob(jobSearch.searchJob(search)); 
                        
                        break;

                    case "afj":
                        System.out.print("Enter the job ID: ");
                        String jobId = in.nextLine();
                        jobSearch.addFavJob(jobId);
                       
    
                        break;

                    case "dfj":
                        System.out.print("Enter the job ID: ");
                        jobId = in.nextLine();
                        jobSearch.removeFavJob(jobId);
                       
    
                        break;

                    case "vfj":

                        jobPrinter.printJob(jobSearch.getFavJobList());

                        break;

                    case "quit":

                        running = false;

                        break;

                    //if the user input is not in the case        
                    default:
                        System.out.println(input + " is not a valid command");
                        continue;
                }
                }catch (Exception e) {
                    
                    }


            
            }

    }
    
    //this method will check if a string is a number, empty string, or not a number
    //this method receives input of string and return boolean
    public static boolean isNumberOrEmptyString(String number) {
        if (number.equals("")) {
            return true;
        }
        else if (!number.matches("^[0-9]*$")) {
            return false;
        }
        else {
            return true;
        }
    }

    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public String nextLine() throws IOException{
            return reader.readLine();
        }

    }
    
}