package Test;

import JobSearch.*;
import Loader.JobLoader;
import Printer.JobPrinter;

public class JobSearchTest {

    public static void main(String[] args) {
        
        try {
            JobLoader jobLoader = new JobLoader();
            JobSearch jobSearch = jobLoader.loadJob();
            JobPrinter jobPrinter = new JobPrinter();
            System.out.println("Testing adding job...");
            //test add job
            if(jobSearch.getJobList().size() == 3) {
                System.out.println("Adding job Test Success!");
            } else {
                System.out.println("Adding job Test Failed");
                System.exit(-1);
            }
            System.out.println();
            
            System.out.println("Testing print job...");
            //test print all job
            jobPrinter.printJob(jobSearch.getJobList());
            //test add fav job with correct id
            System.out.println("Testing adding favourite job...");
            jobSearch.addFavJob("1");
            if (jobSearch.getFavJobList().size() == 1) {
                System.out.println("Adding fav job with correct id Test Success!");
            } else {
                System.out.println("Adding fav job with correct id Test Failed");
                System.exit(-1);
            }
            System.out.println();

            //test add fav job with correct id
            jobSearch.addFavJob("2");
            if (jobSearch.getFavJobList().size() == 2) {
                System.out.println("Adding fav job with correct id Test Success!");
            } else {
                System.out.println("Adding fav job with correct id Test Failed");
                System.exit(-1);
            }
            System.out.println();

            System.out.println("Testing adding favourite job with same id...");
            //test add fav job with same id
            jobSearch.addFavJob("1"); 
            if (jobSearch.getFavJobList().size() == 2) {
                System.out.println("Adding fav job with same id Test Success!");
            } else {
                System.out.println("Adding fav job with same id Test Failed");
                System.exit(-1);
            }
            System.out.println();

            System.out.println("Testing adding favourite job with wrong id...");
            //test add fav job with wrong id
            jobSearch.addFavJob("4"); 
            if (jobSearch.getFavJobList().size() == 2) {
                System.out.println("Adding fav job with wrong id Test Success!");
            } else {
                System.out.println("Adding fav job with wrong id Test Failed");
                System.exit(-1);
            }
            System.out.println();
            
            System.out.println("Testing removing favourite job...");
            //test remove job with correct id
            jobSearch.removeFavJob("1"); 
            if (jobSearch.getFavJobList().size() == 1) {
                System.out.println("Removing fav job with correct id Test Success!");
            } else {
                System.out.println("Removing fav job with correct id Test Failed");
                System.exit(-1);
            }
            System.out.println();

            System.out.println("Testing removing favourite job with wrong id....");
            //test remove job with wrong id
            jobSearch.removeFavJob("3"); 
            if (jobSearch.getFavJobList().size() == 1) {
                System.out.println("Removing fav job with wrong id Test Success!");
            } else {
                System.out.println("Removing fav job with wrong id Test Failed");
                System.exit(-1);
            }
            System.out.println();

            System.out.println("Testing printing favourite job....");
            jobPrinter.printJob(jobSearch.getFavJobList());
            System.out.println();

            //test for search
            System.out.println("Testing searching job with position and salary....");
            String[] posSalary = {"Software Engineer","10000000",""};
            for (Job job : jobSearch.searchJob(posSalary).values() ) {
                if (job.getPosition().equalsIgnoreCase(posSalary[0]) && job.getSalary() >= Double.parseDouble(posSalary[1])) {
                    continue;
                } else {
                    System.out.println("Testing searching job with position and salary Failed");
                    System.exit(-1);
                }
            }
            System.out.println("Testing searching job with position and salary Success");
            System.out.println();

            System.out.println("Testing searching job with position and city....");
            String[] posCity = {"Software Engineer","0","Central Jakarta"};
            for (Job job : jobSearch.searchJob(posCity).values() ) {
                if (job.getPosition().equalsIgnoreCase(posCity[0]) && job.getLocation().getCity().equalsIgnoreCase(posCity[2])) {
                    continue;
                } else {
                    System.out.println("Testing searching job with position and city Failed");
                    System.exit(-1);
                }
            }
            System.out.println("Testing searching job with position and city Success");
            System.out.println();

            System.out.println("Testing searching job with city and salary....");
            String[] citySalary = {"","1000000","Central Jakarta"};
            for (Job job : jobSearch.searchJob(citySalary).values() ) {
                if (job.getSalary() >= Double.parseDouble(citySalary[1]) && job.getLocation().getCity().equalsIgnoreCase(citySalary[2])) {
                    continue;
                } else {
                    System.out.println("Testing searching job with city and salary Failed");
                    System.exit(-1);
                }
            }
            System.out.println("Testing searching job with city and salary Success");
            System.out.println();

            System.out.println("Testing searching job with position, city and salary....");
            String[] posCitySalary = {"Senior Developer","1000000","Central Jakarta"};
            for (Job job : jobSearch.searchJob(posCitySalary).values() ) {
                if (job.getPosition().equalsIgnoreCase(posCitySalary[0]) && job.getSalary() >= Double.parseDouble(posCitySalary[1]) && job.getLocation().getCity().equalsIgnoreCase(posCitySalary[2])) {
                    continue;
                } else {
                    System.out.println("Testing searching job with position, city and salary Failed");
                    System.exit(-1);
                }
            }
            System.out.println("Testing searching job with position, city and salary Success");
            System.out.println();

            System.out.println("Testing searching job with position....");
            String[] pos = {"Senior Developer","0",""};
            for (Job job : jobSearch.searchJob(pos).values() ) {
                if (job.getPosition().equalsIgnoreCase(pos[0])) {
                    continue;
                } else {
                    System.out.println("Testing searching job with position Failed");
                    System.exit(-1);
                }
            }
            System.out.println("Testing searching job with position Success");
            System.out.println();

            System.out.println("Testing searching job with salary....");
            String[] salary = {"","10000000",""};
            for (Job job : jobSearch.searchJob(salary).values() ) {
                if (job.getSalary() >= Double.parseDouble(salary[1]) ) {
                    continue;
                } else {
                    System.out.println("Testing searching job with salary Failed");
                    System.exit(-1);
                }
            }
            System.out.println("Testing searching job with salary Success");
            System.out.println();

            System.out.println("Testing searching job with city....");
            String[] city = {"","0","South Jakarta"};
            for (Job job : jobSearch.searchJob(city).values() ) {
                if (job.getLocation().getCity().equalsIgnoreCase(city[2]) ) {
                    continue;
                } else {
                    System.out.println("Testing searching job with city Failed");
                    
                }
            }
            System.out.println("Testing searching job with city Success");
            System.out.println();

            System.out.println("Testing searching job without input....");
            String[] noInput = {"","0",""};
            if (jobSearch.searchJob(noInput).size() == jobSearch.getJobList().size()) {
                System.out.println("Testing searching job without input Success");
            } else {
                System.out.println("Testing searching job without input Failed");
                System.exit(-1);
            }
            


        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}